//
//  AppDelegate.m
//  NxtWallet
//

#import "AppDelegate.h"
#import "NSURLRequest+IgnoreSSL.h"
#import "ASIHTTPRequest.h"
#import "VersionComparator.h"
#import "NSApplication+Relaunch.h"
#include <sys/stat.h>
#import <CommonCrypto/CommonDigest.h>

@implementation AppDelegate
{
    NSTask *_server;
    NSTask *_minter;
    NSString *_serverPort;
    NSMutableArray *_serverOutput;
    NSMutableArray *_minterOutput;
    
    NSPipe *_pipe;

    NSString *_javaPath;
    NSString *_javaDMGPath;
    NSString *_nxtZipPath;
    
    NSString *_currentVersion;
    NSUserDefaults *_preferences;
    NSString *_downloadHash;
    
    NSString *_savePath;
    NSString *_javaCommand;
    NSString *_mintCommand;
    
    BOOL _started;
    BOOL _isTestNet;
    BOOL _manuallyCheckingForUpdates;
    BOOL _afterServerStarted;
    BOOL _nxtInitializationError;
    
    ASIHTTPRequest *_otherRequest;
    
    NSString *_downloadType;
    double _totalBytes;
    double _downloadedBytes;
    
   // WebInspector *_inspector;
    
    BOOL _showQuitAlert;
}

//- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
- (void)awakeFromNib
{
    [self initVariables];
    [self setSavePath];
    [self generateConfig];
    [self registerDefaults];
    [self setUpWebView];
}

- (void)initVariables
{
    _serverOutput = [[NSMutableArray alloc] init];
    _minterOutput = [[NSMutableArray alloc] init];
    _afterServerStarted = NO;
    _isTestNet = NO;
    _serverPort = @"7876";
    _started = NO;
    _showQuitAlert = NO;
    _nxtInitializationError = NO;
    _javaPath = [self getJavaPath];
}

- (void)setSavePath
{
    NSString *applicationSupportFile = [@"~/Library/Application Support/" stringByExpandingTildeInPath];
    
    _savePath = [NSString pathWithComponents:[NSArray arrayWithObjects:applicationSupportFile, @"Nxt Wallet", @"", nil]];
}

- (void)registerDefaults
{
    NSDictionary *defaults = [NSDictionary dictionaryWithObjectsAndKeys:_savePath, @"WebDatabaseDirectory", nil];
    
    _preferences = [NSUserDefaults standardUserDefaults];
    [_preferences registerDefaults:defaults];
}

- (void)killAlreadyRunningProcessAndRestart
{
    [self killAlreadyRunningProcess:nil];
    [self restartApplication];
}

- (void)killAlreadyRunningProcess:(NSString *)type
{
    int pid;
    
    if (type == nil || [type isEqualToString:@"minter"]) {
        pid = [self getAlreadyRunningProcess:_mintCommand];
        
        if (pid) {
            kill(pid, SIGKILL);
        }
    }
    
    if (type == nil || [type isEqualToString:@"java"]) {
        pid = [self getAlreadyRunningProcess:_javaCommand];
    
        if (pid) {
            kill(pid, SIGKILL);
        }
    }
}

- (int)getAlreadyRunningProcess:(NSString *)command
{
    if (!command) {
        command = _javaCommand;
        if (!_javaCommand) {
            return 0;
        }
    }
    
    NSTask *task;
    task = [[NSTask alloc] init];
    [task setLaunchPath: @"/bin/sh"];
    
    NSArray *arguments;
    arguments = [NSArray arrayWithObjects: @"-c", [NSString stringWithFormat:@"/bin/ps aux | grep '%@' | awk '{print $2}'", [NSRegularExpression escapedPatternForString:command]], nil];
    
    [task setArguments: arguments];
    
    NSPipe *pipe;
    pipe = [NSPipe pipe];
    [task setStandardOutput: pipe];
    
    NSFileHandle *file;
    file = [pipe fileHandleForReading];
    
    [task launch];
    
    NSData *data;
    data = [file readDataToEndOfFile];
    
    int pid = [[[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding] intValue];
    
    return pid;
}

- (IBAction)checkForUpdates:(id)sender
{
    if (!_started || _nxtInitializationError || _showQuitAlert) {
		_manuallyCheckingForUpdates = NO;
		return;
	}
    
    [self hideAlerts];
    
    if (sender && ![sender isKindOfClass:[NSTimer class]]) {
        _manuallyCheckingForUpdates = YES;
    }
    
    if (_isTestNet) {
		if (_manuallyCheckingForUpdates) {
            [self showAlert:@"To check for updates you need to be connected to the main net, not the test net."];
			_manuallyCheckingForUpdates = NO;
		}
		return;
	}
    
    BOOL downloadBetaUpdates = [_preferences boolForKey:@"downloadBetaUpdates"];
    
    if (downloadBetaUpdates) {
        _otherRequest = nil;
        
        [self setBigFetchRequest:[ASIHTTPRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://localhost:%@/nxt?requestType=getAlias&aliasName=nrsversion", _serverPort]]]];
        [[self bigFetchRequest] setDelegate:self];
        [[self bigFetchRequest] setDidFinishSelector:@selector(handleBiggestVersion:)];
        [[self bigFetchRequest] setDidFailSelector:@selector(handleBiggestVersion:)];
        [[self bigFetchRequest] startSynchronous];

        [self setBigFetchRequest2:[ASIHTTPRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://localhost:%@/nxt?requestType=getAlias&aliasName=nrsbetaversion", _serverPort]]]];
        [[self bigFetchRequest2] setDelegate:self];
        [[self bigFetchRequest2] setDidFinishSelector:@selector(handleBiggestVersion:)];
        [[self bigFetchRequest2] setDidFailSelector:@selector(handleBiggestVersion:)];
        [[self bigFetchRequest2] startSynchronous];
    } else {
        [self setBigFetchRequest:[ASIHTTPRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://localhost:%@/nxt?requestType=getAlias&aliasName=nrsversion", _serverPort]]]];
        [[self bigFetchRequest] setDelegate:self];
        [[self bigFetchRequest] setDidFinishSelector:@selector(checkForUpdatesCompleted:)];
        [[self bigFetchRequest] setDidFailSelector:@selector(checkForUpdatesFailed:)];
        [[self bigFetchRequest] startAsynchronous];
    }
}

- (void)downloadUpdateType:(NSString *)type
{
    _manuallyCheckingForUpdates = YES;
    
    if ([type isEqualToString:@"release"]) {
        [self setBigFetchRequest:[ASIHTTPRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://localhost:%@/nxt?requestType=getAlias&aliasName=nrsversion", _serverPort]]]];
        [[self bigFetchRequest] setDelegate:self];
        [[self bigFetchRequest] setDidFinishSelector:@selector(checkForUpdatesCompleted:)];
        [[self bigFetchRequest] setDidFailSelector:@selector(checkForUpdatesFailed:)];
        [[self bigFetchRequest] startAsynchronous];
    } else {
        [self setBigFetchRequest2:[ASIHTTPRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://localhost:%@/nxt?requestType=getAlias&aliasName=nrsbetaversion", _serverPort]]]];
        [[self bigFetchRequest2] setDelegate:self];
        [[self bigFetchRequest2] setDidFinishSelector:@selector(checkForUpdatesCompleted:)];
        [[self bigFetchRequest2] setDidFailSelector:@selector(checkForUpdatesFailed:)];
        [[self bigFetchRequest2] startSynchronous];
    }
}

- (void)handleBiggestVersion:(ASIHTTPRequest *)request
{
    if (!_otherRequest) {
        _otherRequest = request;
        return;
    }

    NSError *error = [request error];
    
    if (error) {
        [self checkForUpdatesCompleted:_otherRequest];
    } else if ([_otherRequest error]) {
        [self checkForUpdatesCompleted:request];
    } else {
        NSString *response1 = [request responseString];
                
        NSRange  searchedRange1 = NSMakeRange(0, [response1 length]);
        NSString *pattern1 = @"aliasURI\":\"([0-9\\.]+[a-zA-Z]?)";
        
        NSRegularExpression *regex1 = [NSRegularExpression regularExpressionWithPattern:pattern1 options:0 error:&error];
        NSTextCheckingResult *match1 = [regex1 firstMatchInString:response1 options:0 range: searchedRange1];
        
        NSString *versionNr1 = [response1 substringWithRange:[match1 rangeAtIndex:1]];
        
        NSString *response2 = [_otherRequest responseString];
        
        NSRange  searchedRange2 = NSMakeRange(0, [response2 length]);
        NSString *pattern2 = @"aliasURI\":\"([0-9\\.]+[a-zA-Z]?)";
        
        NSRegularExpression *regex2 = [NSRegularExpression regularExpressionWithPattern:pattern2 options:0 error:&error];
        NSTextCheckingResult *match2 = [regex2 firstMatchInString:response2 options:0 range: searchedRange2];
        
        NSString *versionNr2 = [response2 substringWithRange:[match2 rangeAtIndex:1]];
        
        BOOL greater = [VersionComparator isVersion:versionNr1 greaterThanVersion:versionNr2];
        
        if (greater) {
            [self checkForUpdatesCompleted:request];
        } else {
            [self checkForUpdatesCompleted:_otherRequest];
        }
    }
}

- (void)checkForUpdatesCompleted:(ASIHTTPRequest *)request
{
    NSError *error = [request error];
    if (!error) {
        NSString *response = [request responseString];
        
        NSRange  searchedRange = NSMakeRange(0, [response length]);
        NSString *pattern = @"aliasURI\":\"([0-9\\.]+[a-zA-Z]?)";
        NSError  *error = nil;
        
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:&error];
        NSTextCheckingResult *match = [regex firstMatchInString:response options:0 range: searchedRange];
        
        NSString *versionNr = [response substringWithRange:[match rangeAtIndex:1]];
        
        _downloadHash = nil;
        
        NSRange spaceRange = [response rangeOfString:@" "];
        if (spaceRange.location != NSNotFound) {
            _downloadHash = [[response substringFromIndex:spaceRange.location] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            if ([_downloadHash length] >= 64) {
                _downloadHash = [_downloadHash substringToIndex:64];
            } else {
                _downloadHash = nil;
            }
        }
        
        BOOL greater = [VersionComparator isVersion:versionNr greaterThanVersion:@"1.2.8"];
        
        if (!greater) {
            if (_manuallyCheckingForUpdates) {
                [self showAlert:@"Try again in a little bit, the blockchain is still downloading..."];
                _manuallyCheckingForUpdates = NO;
            }
            return;
        }
        
        if (versionNr && ![versionNr isEqualToString:@""]) {
            greater = [VersionComparator isVersion:versionNr greaterThanVersion:_currentVersion];
            
            if (greater) {
                if (_manuallyCheckingForUpdates && !_downloadHash) {
                    [self showAlert:@"The hash was not found, the update will not proceed."];
                } else {
                    [self showUpdateNotice:versionNr];
                }
            } else if (_manuallyCheckingForUpdates) {
                [self showAlert:[NSString stringWithFormat:@"You are already using the latest version of the Nxt client (%@).", versionNr]];
            }
        } else {
            [self showAlert:@"Update information was not found, please try again later."];
        }
    }
    
    _manuallyCheckingForUpdates = NO;
}

- (NSString *)escapeStringForJavascript:(NSString *)string
{
    string = [string stringByReplacingOccurrencesOfString:@"\\" withString:@"\\\\"];
    string = [string stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
    string = [string stringByReplacingOccurrencesOfString:@"\'" withString:@"\\\'"];
    string = [string stringByReplacingOccurrencesOfString:@"\n" withString:@"\\n"];
    string = [string stringByReplacingOccurrencesOfString:@"\r" withString:@"\\r"];
    string = [string stringByReplacingOccurrencesOfString:@"\f" withString:@"\\f"];
    
    return string;
}

- (void)hideAlerts
{
    [_webView stringByEvaluatingJavaScriptFromString:@"bootbox.hideAll()"];
}

- (void)showAlert:(NSString *)msg
{
    if (_showQuitAlert) {
        return;
    }
    
    [self hideAlerts];
    
    [_webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"bootbox.alert('%@');", [self escapeStringForJavascript:msg]]];
}

- (void)showAlertAndQuit:(NSString *)msg
{
    if (_showQuitAlert) {
        return;
    }
    
    [self hideAlerts];
    
    if (msg == nil) {
        msg = @"An error occurred, the server has quit. Please restart the application.";
    }
    
    [_webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"bootbox.alert('%@', function() { App.quit(); });", [self escapeStringForJavascript:msg]]];
    
    _showQuitAlert = true;
}

- (void)showInitializationAlert:(NSString *)msg
{
    if (_nxtInitializationError) {
        return;
    }
    
    _nxtInitializationError = true;
    
    [_webView stringByEvaluatingJavaScriptFromString:@"document.getElementById('loading_indicator_container').style.display='none';"];
    [_webView stringByEvaluatingJavaScriptFromString:@"document.getElementById('server_output').innerHTML = 'Exception occurred';"];
    
    [self showAlertAndQuit:msg];
}

- (void)showAlreadyRunningProcessAlert
{
    if (_showQuitAlert) {
        return;
    }
    
    _nxtInitializationError = true;

    [[_webView windowScriptObject] callWebScriptMethod:@"showAlreadyRunningProcessAlert" withArguments:nil];
}

- (void)checkForUpdatesFailed:(ASIHTTPRequest *)request
{
    if (_manuallyCheckingForUpdates) {
        [self showAlert:@"Could not connect to the update server, please try again later.'"];
		_manuallyCheckingForUpdates = NO;
	}
}

- (void)showUpdateNotice:(NSString *)versionNr
{
    if (_showQuitAlert) {
        return;
    }
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://bitbucket.org/JeanLucPicard/nxt/downloads/nxt-client-%@.changelog.txt.asc", versionNr]]];
    [request startSynchronous];
    
    NSError *error = [request error];
    
    if (!error) {
        NSString *changelog = [request responseString];
        [[_webView windowScriptObject] callWebScriptMethod:@"showUpdateNotice" withArguments:[NSArray arrayWithObjects:versionNr, changelog, nil]];
    } else {
        [[_webView windowScriptObject] callWebScriptMethod:@"showUpdateNotice" withArguments:[NSArray arrayWithObjects:versionNr, @"", nil]];
    }
}

- (void)downloadNxt:(NSString *)version
{
    if (_showQuitAlert) {
        return;
    }
    
    _downloadType = @"nxt";
    
    _nxtZipPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Downloads"] stringByAppendingPathComponent:@"nxt.zip"];
    
    [self setBigFetchRequest:[ASIHTTPRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://bitbucket.org/JeanLucPicard/nxt/downloads/nxt-client-%@.zip", version]]]];
    
    [[self bigFetchRequest] setDownloadDestinationPath:_nxtZipPath];
    [[self bigFetchRequest] setTemporaryFileDownloadPath:[NSString stringWithFormat:@"%@.download", _nxtZipPath]];
	[[self bigFetchRequest] setAllowResumeForFileDownloads:NO];
	[[self bigFetchRequest] setDelegate:self];
	[[self bigFetchRequest] setDidFinishSelector:@selector(installNxt:)];
	[[self bigFetchRequest] setDidFailSelector:@selector(installNxtFailed:)];
	[[self bigFetchRequest] setDownloadProgressDelegate:self];
    [[self bigFetchRequest] startAsynchronous];
}

- (void)installNxt:(ASIHTTPRequest *)request
{
    NSData *zipData = [NSData dataWithContentsOfFile:_nxtZipPath];
    
    unsigned char result[CC_SHA256_DIGEST_LENGTH];
    
    CC_SHA256(zipData.bytes, (CC_LONG) zipData.length, result);
    
    NSMutableString *hash = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH*2];
    
    for(int i = 0; i<CC_SHA256_DIGEST_LENGTH; i++)
    {
        [hash appendFormat:@"%02x", result[i]];
    }
        
    if (![hash isEqualToString:_downloadHash]) {
        [self showAlert:@"The hash of the downloaded update does not equal the one supplied by the blockchain. Aborting update."];
        return;
    }

    [self stopServer];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSString *tempFolder = [NSTemporaryDirectory() stringByAppendingPathComponent:@"nxt"];
    
    NSString *destinationFolder = [[NSBundle mainBundle] pathForResource:@"nxt" ofType:nil];
    
    NSDirectoryEnumerator *en = [fileManager enumeratorAtPath:tempFolder];
    
    BOOL res;
    
    NSString *file;
    
    while (file = [en nextObject]) {
        res = [fileManager removeItemAtPath:[tempFolder stringByAppendingPathComponent:file] error:nil];
    }
    
    NSTask *unzipTask = [[NSTask alloc] init];
    
    [unzipTask setLaunchPath:@"/usr/bin/ditto"];

    [unzipTask setArguments:[NSArray arrayWithObjects:@"-v",@"-x",@"-k", @"--rsrc", _nxtZipPath, tempFolder, nil]];

    NSPipe *pipe = [NSPipe pipe];
    
    [unzipTask setStandardOutput:pipe];
    
    [unzipTask launch];
    
    NSData *extract = [[pipe fileHandleForReading] readDataToEndOfFile];
    
    [unzipTask waitUntilExit];
    
    tempFolder = [tempFolder stringByAppendingPathComponent:@"nxt"];
    
    en = [fileManager enumeratorAtPath:tempFolder];
    
    while (file = [en nextObject]) {
        res = [fileManager removeItemAtPath:[destinationFolder stringByAppendingPathComponent:file] error:nil];
        res = [fileManager moveItemAtPath:[tempFolder stringByAppendingPathComponent:file] toPath:[destinationFolder stringByAppendingPathComponent:file] error:nil];
    }
    
    [NSApp relaunch:self];
}

- (void)installNxtFailed:(ASIHTTPRequest *)request
{
    [self showAlert:@"Could not connect to the update server, please try again later."];
}

- (void)noJavaInstalled
{
    if (_showQuitAlert) {
        return;
    }
    
    [_webView stringByEvaluatingJavaScriptFromString:@"showNoJavaInstalledWindow()"];
}

- (void)downloadJava
{
    _downloadType = @"java";
    
    _javaDMGPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Downloads"] stringByAppendingPathComponent:@"nxt-java-install.dmg"];
    
    [self setBigFetchRequest:[ASIHTTPRequest requestWithURL:[NSURL URLWithString:@"http://www.oracle.com/technetwork/java/javase/downloads/jre7-downloads-1880261.html"]]];
    [[self bigFetchRequest] setUseCookiePersistence:NO];
    [[self bigFetchRequest] addRequestHeader:@"Accept" value:@"Accept:text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"];
    [[self bigFetchRequest] addRequestHeader:@"Accept-Language" value:@"en-us"];
    [[self bigFetchRequest] addRequestHeader:@"User-Agent" value:@"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/537.75.14"];
    [[self bigFetchRequest] startSynchronous];
    
    NSString *response = [[self bigFetchRequest] responseString];
    
    NSError *error = nil;
    NSString *pattern = @"http://download\\.oracle\\.com/otn\\-pub/java/jdk/[a-z0-9\\-/]+macosx\\-x64\\.dmg";
    NSRange searchedRange = NSMakeRange(0, [response length]);

    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:&error];
    NSTextCheckingResult *match = [regex firstMatchInString:response options:0 range: searchedRange];
    
    NSString *downloadUrl = [response substringWithRange:[match rangeAtIndex:0]];
        
    if (![downloadUrl length]) {
        downloadUrl = @"http://download.oracle.com/otn-pub/java/jdk/7u72-b14/jre-7u72-macosx-x64.dmg";
    }
    
    downloadUrl = [downloadUrl stringByReplacingOccurrencesOfString:@"http://" withString:@"https://"];
    downloadUrl = [downloadUrl stringByReplacingOccurrencesOfString:@"download.oracle.com" withString:@"edelivery.oracle.com"];
    
    NSDate *past = [NSDate date];
    NSTimeInterval oldTime = [past timeIntervalSince1970];
    NSString *timestamp = [[NSString alloc] initWithFormat:@"%0.0f", oldTime * 1000 - 5];

    NSMutableArray *cookies = [[self bigFetchRequest] requestCookies];

    [cookies addObject:[self createCookie:@"s_nr" withValue:timestamp]];
    [cookies addObject:[self createCookie:@"gpw_e24" withValue:@"http%3A%2F%2Fwww.oracle.com%2Ftechnetwork%2Fjava%2Fjavase%2Fdownloads%2Fjre7-downloads-1880261.html"]];
    [cookies addObject:[self createCookie:@"s_cc" withValue:@"true"]];
    [cookies addObject:[self createCookie:@"s_sq" withValue:@"%5B%5BB%5D%5D"]];
    [cookies addObject:[self createCookie:@"oraclelicense" withValue:@"accept-securebackup-cookie"]];

    [self setBigFetchRequest:[ASIHTTPRequest requestWithURL:[NSURL URLWithString:downloadUrl]]];
    [[self bigFetchRequest] setUseCookiePersistence:NO];
    [[self bigFetchRequest] setRequestCookies:cookies];
    [[self bigFetchRequest] addRequestHeader:@"Referer" value:@"http://www.oracle.com/technetwork/java/javase/downloads/jre7-downloads-1880261.html"];
    [[self bigFetchRequest] addRequestHeader:@"Accept" value:@"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"];
    [[self bigFetchRequest] addRequestHeader:@"Accept-Language" value:@"en-us"];
    [[self bigFetchRequest] addRequestHeader:@"User-Agent" value:@"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/537.75.14"];
    [[self bigFetchRequest] setDownloadDestinationPath:_javaDMGPath];
    [[self bigFetchRequest] setTemporaryFileDownloadPath:[NSString stringWithFormat:@"%@.download", _javaDMGPath]];
	[[self bigFetchRequest] setAllowResumeForFileDownloads:NO];
	[[self bigFetchRequest] setDelegate:self];
	[[self bigFetchRequest] setDidFinishSelector:@selector(downloadJavaComplete:)];
	[[self bigFetchRequest] setDidFailSelector:@selector(downloadJavaFailed:)];
    [[self bigFetchRequest] setDownloadProgressDelegate:self];
    [[self bigFetchRequest] startAsynchronous];
}

- (NSHTTPCookie *)createCookie:(NSString *)name withValue:(NSString *)value
{
    NSDictionary *properties = [[NSMutableDictionary alloc] init];
    [properties setValue:value forKey:NSHTTPCookieValue];
    [properties setValue:name forKey:NSHTTPCookieName];
    [properties setValue:@".oracle.com" forKey:NSHTTPCookieDomain];
    [properties setValue:[NSDate dateWithTimeIntervalSinceNow:60*60] forKey:NSHTTPCookieExpires];
    [properties setValue:@"/" forKey:NSHTTPCookiePath];
    NSHTTPCookie *cookie = [[NSHTTPCookie alloc] initWithProperties:properties];
    
    return cookie;
}

- (void)downloadJavaComplete:(ASIHTTPRequest *)request
{
    [[NSWorkspace sharedWorkspace] openFile:_javaDMGPath];
    [self quitApp];
}

- (void)downloadJavaFailed:(ASIHTTPRequest *)request
{
    [self showAlertAndQuit:@"Download failed, please <a href='http://www.java.com/en/download/manual.jsp'>click here</a> to download and install java manually."];
}

-(void)request:(ASIHTTPRequest *)request didReceiveBytes:(long long)bytes
{
    _downloadedBytes += bytes;
    
    long progress = (100.0 * _downloadedBytes / _totalBytes);
    
    [_webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"updateDownloadProgress('%@', %ld)", _downloadType, progress]];
}

- (void)request:(ASIHTTPRequest *)request incrementDownloadSizeBy:(long long)newLength
{
    _totalBytes = newLength;
    _downloadedBytes = 0;
}

- (void)setUpWebView
{
   // [[NSURLCache sharedURLCache] setMemoryCapacity:0];
   // [[NSURLCache sharedURLCache] setDiskCapacity:0];
    //[_webView setResourceLoadDelegate:self];
    
    [_webView setPreferencesIdentifier:@"org.nxt.NxtWallet"];
    WebPreferences *preferences = [_webView preferences];
    [preferences setUserStyleSheetEnabled:NO];
    
    [preferences _setLocalStorageDatabasePath:_savePath];
    [preferences setLocalStorageEnabled:YES];
    [preferences setDatabasesEnabled:YES];
    
    [preferences setAutosaves:YES];
    
    NSString *dbPath;
    NSString *localDBPath = [preferences _localStorageDatabasePath];

    //http://stackoverflow.com/questions/4527905/how-do-i-enable-local-storage-in-my-webkit-based-application
    
    if ([WebStorageManager respondsToSelector:@selector(_storageDirectoryPath)]) {
        dbPath = [WebStorageManager _storageDirectoryPath];
    } else {
        dbPath = localDBPath;
    }
    
    //Define application cache quota
    static const unsigned long long defaultTotalQuota = 10 * 1024 * 1024; // 10MB
    static const unsigned long long defaultOriginQuota = 5 * 1024 * 1024; // 5MB
    [preferences setApplicationCacheTotalQuota:defaultTotalQuota];
    [preferences setApplicationCacheDefaultOriginQuota:defaultOriginQuota];
    [preferences setOfflineWebApplicationCacheEnabled:YES];
    [preferences _setLocalStorageDatabasePath:dbPath];
    [preferences setLocalStorageEnabled:YES];
    
    [_webView setPreferences:preferences];
    
    [_webView setPolicyDelegate:self];
    [_webView setFrameLoadDelegate:self];
    [_webView setUIDelegate:self];
    [_webView setResourceLoadDelegate:self];
    
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"index" withExtension:@"html" subdirectory:@"startup"];
    
    [[_webView mainFrame] loadRequest:[NSURLRequest requestWithURL:url]];
}

- (NSArray *)webView:(WebView *)sender contextMenuItemsForElement:(NSDictionary *)element
    defaultMenuItems:(NSArray *)defaultMenuItems
{    
    NSMutableArray *menuItems = [NSMutableArray array];
    
    for (NSMenuItem *menuItem in defaultMenuItems) {
        if ([menuItem tag] == 12 || [menuItem tag] == 7) { //reload and open frame in new window
           //ignore
        } else {
            [menuItems addObject:menuItem];
        }
    }
    
    return menuItems;
}

- (void)webView:(WebView *)sender frame:(WebFrame *)frame exceededDatabaseQuotaForSecurityOrigin:(id) origin database:(NSString *)databaseIdentifier
{
    static const unsigned long long defaultQuota = 5 * 1024 * 1024;
    if ([origin respondsToSelector: @selector(setQuota:)]) {
        [origin performSelector:@selector(setQuota:) withObject:[NSNumber numberWithLongLong: defaultQuota]];
    }
}

- (void)webView:(WebView *)sender decidePolicyForNewWindowAction:(NSDictionary *)actionInformation request:(NSURLRequest *)request newFrameName:(NSString *)frameName decisionListener:(id<WebPolicyDecisionListener>)listener {
    [[NSWorkspace sharedWorkspace] openURL:[actionInformation objectForKey:WebActionOriginalURLKey]];
    [listener ignore];
}

- (void)webView:(WebView *)webView decidePolicyForNavigationAction:(NSDictionary *)actionInformation request:(NSURLRequest *)request frame:(WebFrame *)frame decisionListener:(id <WebPolicyDecisionListener>)listener
{
    NSString *host = [[request URL] host];
    
    if (host && ![host isEqualToString:@"localhost"]) {
        [[NSWorkspace sharedWorkspace] openURL:[request URL]];
    } else {
        [listener use];
    }
}
    
- (void)positionScreen
{
    [_window setFrameAutosaveName:@"nxtClientWindow"];
    [_window makeKeyAndOrderFront:NSApp];
}

- (IBAction)startStopMinting:(id)sender
{
    if (_minter) {
        [_startStopMinting setTitle:@"Start Minting"];
        [self killAlreadyRunningProcess:@"minter"];
        _minter = nil;
        return;
    }
    
    [_startStopMinting setTitle:@"Stop Minting"];
    
    NSTask *minter;
    
    @try {
        minter = [[NSTask alloc] init];
        
        NSString *path = [[NSBundle mainBundle] pathForResource:@"nxt" ofType:nil];
        
        NSString *startCommand = @"classes:lib/*:conf";
        
        NSArray *args  = [NSArray arrayWithObjects:@"-cp", @"classes:lib/*:conf", @"nxt.mint.MintWorker", nil];
        
        _mintCommand = @"java -cp classes:lib/*:conf nxt.mint.MintWorker";
        
        int pid = [self getAlreadyRunningProcess:_mintCommand];
        
        if (pid) {
            [self killAlreadyRunningProcess:@"minter"];
        }
        
        [minter setLaunchPath:_javaPath];
        
        [minter setCurrentDirectoryPath:path];
        [minter setArguments:args];
        
        [minter setStandardOutput:[NSPipe pipe]];
        [minter setStandardError:[NSPipe pipe]];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(readMinterOutput:) name:NSFileHandleReadCompletionNotification object:[[minter standardOutput] fileHandleForReading]];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(readMinterError:) name:NSFileHandleReadCompletionNotification object:[[minter standardError] fileHandleForReading]];
        
        [[[minter standardOutput] fileHandleForReading] readInBackgroundAndNotify];
        [[[minter standardError] fileHandleForReading] readInBackgroundAndNotify];
        
        [minter launch];
        
        _minter = minter;
    } @catch ( NSException *e ) {
        _minter = nil;
        [_startStopMinting setTitle:@"Start Minting"];
        [self showAlert:[e reason]];
    }

}

- (void)startServer
{
    NSTask *server;
    
    @try {
        server = [[NSTask alloc] init];
        
        NSString *path = [[NSBundle mainBundle] pathForResource:@"nxt" ofType:nil];
        
        NSArray *args  = [NSArray arrayWithObjects:@"-Xmx1024M", @"-cp", @"classes:lib/*:conf", @"nxt.Nxt", nil];
        
        _javaCommand = [NSString stringWithFormat:@"java -Xmx1024M -cp classes:lib/*:conf nxt.Nxt"];

        int pid = [self getAlreadyRunningProcess:_javaCommand];
        
        if (pid) {
            [self showAlreadyRunningProcessAlert];
            return;
        }
        
        [server setLaunchPath:_javaPath];
        
        [server setCurrentDirectoryPath:path];
        [server setArguments:args];
            
        [server setStandardOutput:[NSPipe pipe]];
        [server setStandardError:[NSPipe pipe]];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(readServerOutput:) name:NSFileHandleReadCompletionNotification object:[[server standardOutput] fileHandleForReading]];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(readServerError:) name:NSFileHandleReadCompletionNotification object:[[server standardError] fileHandleForReading]];
    
        [[[server standardOutput] fileHandleForReading] readInBackgroundAndNotify];
        [[[server standardError] fileHandleForReading] readInBackgroundAndNotify];
    
        [server launch];
        
        _server = server;
    } @catch ( NSException *e ) {
        [self showInitializationAlert:[e reason]];
    }
}

- (void)stopServer
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    if (_server) {
        [_server interrupt];
        [_server waitUntilExit];
    }
    
    if (_minter) {
        [_minter interrupt];
        [_minter waitUntilExit];
    }
    
    [self killAlreadyRunningProcess:nil];
}

- (NSString *)getJavaPath
{
    NSMutableArray *possiblePaths = [[NSMutableArray alloc] init];
    
    [possiblePaths addObject:@"/Library/Internet Plug-Ins/JavaAppletPlugin.plugin/Contents/Home/bin/java"];
    
    @try {
        NSTask *task = [[NSTask alloc] init];
        [task setLaunchPath:@"/usr/libexec/java_home"];
        [task setArguments: [NSArray arrayWithObjects:@"-v 1.7+", nil]];
    
        NSPipe *pipe = [NSPipe pipe];
        [task setStandardOutput:pipe];
        [task setStandardError:pipe];
    
        NSFileHandle *file = [pipe fileHandleForReading];
    
        [task launch];
    
        NSData *data = [file readDataToEndOfFile];
    
        [task waitUntilExit];
    
        NSString *text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
    
        if (![text isEqualToString:@""] && [text rangeOfString:@"Unable"].location == NSNotFound) {
            [possiblePaths addObject:[NSString stringWithFormat:@"%@/bin/java", [text stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]]]];
        }
    }
    @catch (NSException *exception) {
        
    }
    
    for (NSString *possiblePath in possiblePaths) {
        if (![possiblePath isEqualToString:@"java"] && ![[NSFileManager defaultManager] fileExistsAtPath:possiblePath]) {
            continue;
        }
        
        @try {
            NSTask *task = [[NSTask alloc] init];
            [task setLaunchPath:possiblePath];
        
            [task setArguments: [NSArray arrayWithObjects:@"-version", nil]];
            
            //apparently java version is outputted to standard error..?
            
            NSPipe *pipe = [NSPipe pipe];
            [task setStandardError: pipe];
        
            NSFileHandle *file = [pipe fileHandleForReading];
        
            [task launch];
        
            NSData *data = [file readDataToEndOfFile];
        
            [task waitUntilExit];
        
            unsigned int lowerJavaVersion = [self lowerJavaVersionNumberFromJavaFile:data];
            
            if (lowerJavaVersion >= 7) {
                return possiblePath;
            }
        }
        @catch (NSException *exception) {
        }
    }
    
    return nil;
}

- (unsigned int)lowerJavaVersionNumberFromJavaFile:(NSData *)data
{
    NSString *text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
    
    NSRange javaVersionLocation = [text rangeOfString:@"1."];
    unsigned int lowerJavaVersion = [text substringWithRange:NSMakeRange(javaVersionLocation.location + javaVersionLocation.length, 1)].intValue;
    return lowerJavaVersion;
}

- (void)afterServerStarted
{
    _afterServerStarted = YES;
    
    if (_isTestNet) {
        [_switchNet setTitle:@"Switch to Main Net"];
    } else {
        [_switchNet setTitle:@"Switch to Test Net"];
    }
    
    [_webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"document.getElementById('nrs').setAttribute('src', 'http://localhost:%@?app=mac-2.2.0');", _serverPort]];

    [_checkForUpdates setEnabled:YES];
    [self checkForUpdates:nil];
    
    //once a day
    [NSTimer scheduledTimerWithTimeInterval:86400 target:self selector:@selector(checkForUpdates:) userInfo:nil repeats:YES];
}

- (void)webView:(WebView *)sender didFinishLoadForFrame:(WebFrame *)frame
{
    NSScrollView *mainScrollView = sender.mainFrame.frameView.documentView.enclosingScrollView;
    [mainScrollView setVerticalScrollElasticity:NSScrollElasticityNone];
    [mainScrollView setHorizontalScrollElasticity:NSScrollElasticityNone];
    
    NSString *url = [[[[frame dataSource] request] URL] absoluteString];
    
    if (_started || [url isEqualToString:@"about:blank"]) {
        return;
    }
    
    [self positionScreen];

    [[_webView windowScriptObject] setValue:self forKey:@"App"];
    
    if (!_javaPath) {
        [self noJavaInstalled];
    } else {
        [self startServer];
    }
}

- (BOOL)validateMenuItem:(NSMenuItem *)menuItem
{
    if (menuItem && [menuItem action] && [menuItem action] == @selector(checkForUpdates:)) {
        return _afterServerStarted;
    }
    
    return YES;
}

- (void)readServerOutput:(NSNotification *)notification
{
    NSData *data = [[notification userInfo] objectForKey:NSFileHandleNotificationDataItem];
    
    if ([data length]) {
        NSString *text = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        [self logServerOutput:text];
        [self checkServerOutput:text];
        
        [[notification object] readInBackgroundAndNotify];
    } else {
        [self showInitializationAlert:nil];
    }
}

- (void)readServerError:(NSNotification *)notification
{
    NSData *data = [[notification userInfo] objectForKey:NSFileHandleNotificationDataItem];
   
    if ([data length]) {
        NSString *text = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        [self logServerOutput:text];
        [self checkServerOutput:text];
        
        if (!_nxtInitializationError) {
            if ([text rangeOfString:@"java.net.BindException"].location != NSNotFound) {
                [self showInitializationAlert:@"Server port already in use. Make sure you are not already running the NRS server in another process."];
            } else if ([text rangeOfString:@"(Nxt.java:"].location != NSNotFound || [text rangeOfString:@"Failed to start"].location != NSNotFound) {
                [self showInitializationAlert:nil];
            }
        }
        
        [[notification object] readInBackgroundAndNotify];
    } else {
        [self showInitializationAlert:nil];
    }
}

- (void)checkServerOutput:(NSString *)text
{
    if (!_currentVersion) {
        NSRange  searchedRange = NSMakeRange(0, [text length]);
        NSString *pattern = @"server version ([0-9\\.]+[a-zA-Z]?)";
        NSError  *error = nil;
        
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:&error];
        NSTextCheckingResult *match = [regex firstMatchInString:text options:0 range: searchedRange];
        
        NSString *versionNr = [text substringWithRange:[match rangeAtIndex:1]];
        
        if (versionNr && ![versionNr isEqualToString:@""]) {
            _currentVersion = versionNr;
        }
    }
        
    if (!_started) {
        if ([text rangeOfString:@"nxt.isTestnet = \"true\""].location != NSNotFound) {
            _isTestNet = YES;
            _serverPort = @"6876";
        } else if ([text rangeOfString:@"nxt.isTestnet = \"false\""].location != NSNotFound) {
            _isTestNet = NO;
            _serverPort = @"7876";
        }
        
        
        if ([text rangeOfString:@"started successfully"].location != NSNotFound) {
            [self performSelector:@selector(afterServerStarted) withObject:nil afterDelay:5.0f];
            _started = YES;
        } else {
            [self checkForErrors:text];
        }
    }
}

- (void)logServerOutput:(NSString *)text
{    
    [_serverOutput addObject:text];

    if ([_serverOutput count] > 100) {
        [_serverOutput removeObjectAtIndex:0];
    }
    
    if (_nxtInitializationError) {
		return;
	}

    if (!_started) {
        [[_webView windowScriptObject] callWebScriptMethod:@"logServerOutput" withArguments:[NSArray arrayWithObjects:text, nil]];
	}
}

- (void)checkForErrors:(NSString *)text
{
    if (_nxtInitializationError) {
        return;
    }
    
    NSError *error;

    NSRange  searchedRange = NSMakeRange(0, [text length]);
    NSString *pattern = @"java\\.lang\\.ExceptionInInitializerError|java\\.net\\.BindException";
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:&error];
    NSTextCheckingResult *match = [regex firstMatchInString:text options:0 range: searchedRange];
    
    if (match && [match rangeAtIndex:0].location != NSNotFound) {
        NSString *message;
        
        if ([text rangeOfString:@"java.net.BindException"].location != NSNotFound) {
            message = [NSString stringWithFormat:@"The server address is already in use. Please close any other apps/services that may be running on port %@.", _serverPort];
        } else if ([text rangeOfString:@"Database may be already in use"].location != NSNotFound) {
            message = @"The server database is already in use. Please close any other apps/services that may be connected to this database.";
        } else {
            message = @"A server initialization error occurred.";
        }
        
        [self showInitializationAlert:message];
    }
}

- (void)readMinterOutput:(NSNotification *)notification
{
    NSData *data = [[notification userInfo] objectForKey:NSFileHandleNotificationDataItem];
    
    if ([data length]) {
        NSString *text = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        [self logMinterOutput:text];
    // [self checkServerOutput:text];
        
        [[notification object] readInBackgroundAndNotify];
    }
}

- (void)readMinterError:(NSNotification *)notification
{
    NSData *data = [[notification userInfo] objectForKey:NSFileHandleNotificationDataItem];
    
    if ([data length]) {
        NSString *text = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        [self logMinterOutput:text];
        
        /*
        [self checkServerOutput:text];
        
        if (!_nxtInitializationError) {
            if ([text rangeOfString:@"java.net.BindException"].location != NSNotFound) {
                [self showInitializationAlert:@"Server port already in use. Make sure you are not already running the NRS server in another process."];
            } else if ([text rangeOfString:@"(Nxt.java:"].location != NSNotFound || [text rangeOfString:@"Failed to start"].location != NSNotFound) {
                [self showInitializationAlert:nil];
            }
        }
        */
        
        [[notification object] readInBackgroundAndNotify];
    }
}

- (void)logMinterOutput:(NSString *)text
{
    [_minterOutput addObject:text];
    
    if ([_minterOutput count] > 100) {
        [_minterOutput removeObjectAtIndex:0];
    }
}


- (void)quitApp
{
    [NSApp performSelector:@selector(terminate:) withObject:nil afterDelay:0.0];
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)theApplication
{
    return YES;
}

- (void)applicationWillTerminate:(NSNotification *)aNotification
{
    [self stopServer];
}

- (IBAction)openNxtForumUrl:(id)sender
{
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"https://nxtforum.org/"]];
}

- (IBAction)openWikiUrl:(id)sender
{
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"http://www.thenxtwiki.org"]];
}

- (IBAction)openIRCUrl:(id)sender
{
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"https://kiwiirc.com/client/irc.freenode.org/#nxtalk"]];
}

- (IBAction)openNxtUrl:(id)sender
{
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"http://nxt.org/"]];
}

- (IBAction)showMintingPreferencesWindow:(id)sender;
{
    if (_showQuitAlert) {
        return;
    }
    
    NSError *error = nil;
    
    NSMutableDictionary *mintingPreferences = [self getConfig:@[@"nxt.mint.serverAddress",
                                                                @"nxt.mint.currencyCode",
                                                                @"nxt.mint.secretPhrase",
                                                                @"nxt.mint.unitsPerMint",
                                                                @"nxt.mint.threadPoolSize",
                                                                @"nxt.mint.initialNonce",
                                                                @"nxt.mint.isSubmitted",
                                                                @"nxt.mint.useHttps"]];
    
    if (!mintingPreferences[@"nxt.mint.serverAddress"]) {
        mintingPreferences[@"nxt.mint.serverAddress"] = @"localhost";
    }
    if (!mintingPreferences[@"nxt.mint.currencyCode"]) {
        mintingPreferences[@"nxt.mint.currencyCode"] = @"";
    }
    if (!mintingPreferences[@"nxt.mint.secretPhrase"]) {
        mintingPreferences[@"nxt.mint.secretPhrase"] = @"";
    }
    if (!mintingPreferences[@"nxt.mint.unitsPerMint"]) {
        mintingPreferences[@"nxt.mint.unitsPerMint"] = @"1";
    }
    if (!mintingPreferences[@"nxt.mint.threadPoolSize"]) {
        mintingPreferences[@"nxt.mint.threadPoolSize"] = @"1";
    }
    if (!mintingPreferences[@"nxt.mint.initialNonce"]) {
        mintingPreferences[@"nxt.mint.initialNonce"] = @"0";
    }
    if (!mintingPreferences[@"nxt.mint.isSubmitted"]) {
        mintingPreferences[@"nxt.mint.isSubmitted"] = @"true";
    }
    if (!mintingPreferences[@"nxt.mint.useHttps"]) {
        mintingPreferences[@"nxt.mint.useHttps"] = @"false";
    }
    
    NSData *jsonData= [NSJSONSerialization dataWithJSONObject:mintingPreferences options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    [[_webView windowScriptObject] callWebScriptMethod:@"showMintingPreferencesWindow" withArguments:[NSArray arrayWithObjects:jsonString, nil]];
}

- (void)saveMintingPreferences:(NSString *)returnedData
{
    NSError *error = nil;
    
    id object = [NSJSONSerialization
                 JSONObjectWithData:[returnedData dataUsingEncoding:NSUTF8StringEncoding]
                 options:0
                 error:&error];
        
    if (error || ![object isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    NSDictionary *dict =  @{@"nxt.mint.serverAddress": object[@"serverAddress"],
                            @"nxt.mint.currencyCode": object[@"currencyCode"],
                            @"nxt.mint.secretPhrase": object[@"secretPhrase"],
                            @"nxt.mint.unitsPerMint": object[@"unitsPerMint"],
                            @"nxt.mint.threadPoolSize": object[@"threadPoolSize"],
                            @"nxt.mint.initialNonce": object[@"initialNonce"],
                            @"nxt.mint.isSubmitted": object[@"isSubmitted"],
                            @"nxt.mint.useHttps": object[@"useHttps"]};

    [self addToConfig:dict];
}

- (IBAction)switchNet:(id)sender
{
    [self addToConfig:@{@"nxt.isTestnet": (_isTestNet ? @"false" : @"true")}];
    [self stopServer];
    [NSApp relaunch:self];
}
    
- (void)restartApplication
{
    [NSApp relaunch:self];
}

- (void)generateConfig
{
    NSDictionary *settings = @{@"nxt.dbUrl": [NSString stringWithFormat:@"jdbc:h2:%@/nxt_db/nxt;DB_CLOSE_ON_EXIT=FALSE", _savePath],
                               @"nxt.testDbUrl": [NSString stringWithFormat:@"jdbc:h2:%@/nxt_test_db/nxt;DB_CLOSE_ON_EXIT=FALSE", _savePath]};
    
	[self addToConfig:settings];
}

- (NSString *)getConfigFilePath
{
    NSString *configFile = [[[NSBundle mainBundle] pathForResource:@"nxt" ofType:nil] stringByAppendingPathComponent:@"conf/nxt.properties"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:configFile]) {
        [[NSFileManager defaultManager] createFileAtPath:configFile
                                                contents:[@"" dataUsingEncoding:NSUTF8StringEncoding]
                                              attributes:nil];
    }

    return configFile;
}

- (NSString *)getConfigFile
{
    return [NSString stringWithContentsOfFile:[self getConfigFilePath] encoding:NSUTF8StringEncoding error:nil];
}

- (NSMutableDictionary *)getConfig:(NSArray *)keys
{
    NSMutableDictionary *config = [[NSMutableDictionary alloc] init];
    
    NSString *contents = [self getConfigFile];
    
    NSArray *lines = [contents componentsSeparatedByCharactersInSet: [NSCharacterSet newlineCharacterSet]];
    
    for (NSString *line in lines) {
        NSRange range = [line rangeOfString:@"="];

        if (range.location != NSNotFound) {
            NSString *settingKey = [line substringToIndex:range.location];
            NSString *settingValue = [line substringFromIndex:range.location+range.length];
            
            settingKey   = [settingKey stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            settingValue = [settingValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

            if (!keys || [keys containsObject:settingKey]) {
                [config setObject:settingValue forKey:settingKey];
            }
        }
    }

    return config;
}

- (void)addToConfig:(NSDictionary *)settingsDict
{
    NSMutableDictionary *config = [self getConfig:nil];
    
    for (NSString *settingKey in settingsDict) {
        [config setObject:settingsDict[settingKey] forKey:settingKey];
    }
    
    NSMutableString *newContents = [[NSMutableString alloc] init];
    
    for (NSString *key in config) {
        [newContents appendString:[NSString stringWithFormat:@"%@=%@\r\n", key, config[key]]];
    }
    
    if (![newContents isEqualToString:[self getConfigFile]]) {
        [newContents writeToFile:[self getConfigFilePath]
                      atomically:NO
                        encoding:NSStringEncodingConversionAllowLossy
                           error:nil];
    }
}

- (IBAction)redownloadBlockChain:(id)sender
{
    [self stopServer];
    
    NSArray *filesToRemove;
    
    //todo test
    
    if (_isTestNet) {
        filesToRemove = [NSArray arrayWithObjects:@"nxt_test_db/nxt.h2.db", @"nxt_test_db/nxt.lock.db", @"nxt_test_db/nxt.trace.db", nil];
    } else {
        filesToRemove = [NSArray arrayWithObjects:@"nxt_db/nxt.h2.db", @"nxt_db/nxt.lock.db", @"nxt_db/nxt.trace.db", nil];
    }
    
    
    for (NSString *file in filesToRemove) {
        [[NSFileManager defaultManager] removeItemAtPath:[_savePath stringByAppendingPathComponent:file] error:nil];
    }

    NSData *contents = [@"" dataUsingEncoding:NSUTF8StringEncoding];
    
    [[NSFileManager defaultManager] createFileAtPath:[[[NSBundle mainBundle] pathForResource:@"nxt" ofType:nil] stringByAppendingPathComponent:@"blockchain.nrs"]
                                            contents:contents
                                          attributes:nil];
    
    [self restartApplication];
}

- (IBAction)showPreferencesWindow:(id)sender
{
    if (_showQuitAlert) {
        return;
    }
    
    NSError *error = nil;
    
    NSDictionary *preferences = [NSDictionary dictionaryWithObjectsAndKeys:[_preferences objectForKey:@"checkForUpdates"], @"checkForUpdates", nil];
    NSData *jsonData= [NSJSONSerialization dataWithJSONObject:preferences options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    [[_webView windowScriptObject] callWebScriptMethod:@"showPreferencesWindow" withArguments:[NSArray arrayWithObjects:jsonString, nil]];
}

- (void)savePreferences:(NSString *)returnedData
{
    NSError *error = nil;
    
    id object = [NSJSONSerialization
                 JSONObjectWithData:[returnedData dataUsingEncoding:NSUTF8StringEncoding]
                 options:0
                 error:&error];
    
    if (error || ![object isKindOfClass:[NSDictionary class]]) {
        return;
    }

    [_preferences setInteger:[[object valueForKey:@"checkForUpdates"] intValue] forKey:@"checkForUpdates"];
}

- (void)copyToClipboard:(NSString *)text
{
    NSPasteboard *pasteBoard = [NSPasteboard generalPasteboard];
    [pasteBoard declareTypes:[NSArray arrayWithObjects:NSStringPboardType, nil] owner:nil];
    [pasteBoard setString: text forType:NSStringPboardType];
}

- (void)simulateTabKey
{
    id keyTarget = [[NSApp keyWindow] firstResponder];
    
    
    NSEvent *aKeyDownEvent = [NSEvent keyEventWithType:NSKeyDown
                                              location:NSMakePoint(0,0)
                                         modifierFlags:0
                                             timestamp:[NSDate timeIntervalSinceReferenceDate]
                                          windowNumber:[[NSApp keyWindow] windowNumber]
                                               context:[[NSApp keyWindow] graphicsContext]
                                            characters:@""
                           charactersIgnoringModifiers:@""
                                             isARepeat:NO keyCode:0x30];
    [keyTarget keyDown:aKeyDownEvent];
}

+ (NSString*)webScriptNameForSelector:(SEL)sel
{
    if (sel == @selector(quitApp)) {
        return @"quit";
    } else if (sel == @selector(downloadNxt:)) {
        return @"downloadNxt";
    } else if (sel == @selector(savePreferences:)) {
        return @"savePreferences";
    } else if (sel == @selector(saveMintingPreferences:)) {
        return @"saveMintingPreferences";
    } else if (sel == @selector(downloadJava)) {
        return @"downloadJava";
    } else if (sel == @selector(copyToClipboard:)) {
        return @"copyToClipboard";
    } else if (sel == @selector(downloadUpdateType:)) {
        return @"downloadUpdateType";
    } else if (sel == @selector(simulateTabKey)) {
        return @"simulateTabKey";
    } else if (sel == @selector(killAlreadyRunningProcessAndRestart)) {
        return @"killAlreadyRunningProcessAndRestart";
    }
    
    return nil;
}

+ (BOOL)isSelectorExcludedFromWebScript:(SEL)sel
{
    if (sel == @selector(quitApp)) {
        return NO;
    } else if (sel == @selector(downloadNxt:)) {
        return NO;
    } else if (sel == @selector(savePreferences:)) {
        return NO;
    } else if (sel == @selector(saveMintingPreferences:)) {
        return NO;
    } else if (sel == @selector(downloadJava)) {
        return NO;
    } else if (sel == @selector(copyToClipboard:)) {
        return NO;
    } else if (sel == @selector(downloadUpdateType:)) {
        return NO;
    } else if (sel == @selector(simulateTabKey)) {
        return NO;
    } else if (sel == @selector(killAlreadyRunningProcessAndRestart)) {
        return NO;
    }
    
    return YES;
}

- (IBAction)viewServerLog:(id)sender
{
    if (_showQuitAlert) {
        return;
    }
    
    [[_webView windowScriptObject] callWebScriptMethod:@"viewServerLog" withArguments:[NSArray arrayWithObjects:_serverOutput, nil]];
}

- (IBAction)viewMinterLog:(id)sender
{
    if (_showQuitAlert) {
        return;
    }
    
    [[_webView windowScriptObject] callWebScriptMethod:@"viewMinterLog" withArguments:[NSArray arrayWithObjects:_minterOutput, nil]];

}
@end
