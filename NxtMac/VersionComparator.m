//
//  VersionComparator.m
//
//  Created by Dan Hanly on 12/06/2012.
//  Copyright (c) 2012 Celf Creative. All rights reserved.
//
//

#import "VersionComparator.h"

@implementation VersionComparator

static NSInteger maxValues = 3;

+ (BOOL)isVersion:(NSString *)versionA greaterThanVersion:(NSString *)versionB
{    
    NSMutableArray *versionAArray = [[self normaliseValuesFromArray:[versionA componentsSeparatedByString:@"."]] mutableCopy];
    NSMutableArray *versionBArray = [[self normaliseValuesFromArray:[versionB componentsSeparatedByString:@"."]] mutableCopy];
    
    NSString *lastPartA = [versionAArray objectAtIndex:[versionAArray count]-1];
    NSString *lastPartB = [versionBArray objectAtIndex:[versionBArray count]-1];
    
    NSString *letterA = @"";
    NSString *letterB = @"";
    
    if ([lastPartA length] >= 2) {
        NSString *testNumberA = [lastPartA substringWithRange:NSMakeRange(0, [lastPartA length]-1)];
        NSString *testLetterA = [lastPartA substringWithRange:NSMakeRange([lastPartA length]-1, 1)];
        
        BOOL isLetterA = [[testLetterA stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]] isEqualToString:testLetterA];

        if (isLetterA) {
            [versionAArray setObject:testNumberA atIndexedSubscript:[versionAArray count]-1];
            letterA = testLetterA;
        }
    }
   
    if ([lastPartB length] >= 2) {
        NSString *testNumberB = [lastPartB substringWithRange:NSMakeRange(0, [lastPartB length]-1)];
        NSString *testLetterB = [lastPartB substringWithRange:NSMakeRange([lastPartB length]-1, 1)];
        
        BOOL isLetterB = [[testLetterB stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]] isEqualToString:testLetterB];
        
        if (isLetterB) {
            [versionBArray setObject:testNumberB atIndexedSubscript:[versionBArray count]-1];
            letterB = testLetterB;
        }
    }
    
    for (NSInteger i=0; i<maxValues; i++) {
        if ([[versionAArray objectAtIndex:i] integerValue] > [[versionBArray objectAtIndex:i] integerValue]) {
            return true;
        } else if ([[versionAArray objectAtIndex:i] integerValue] < [[versionBArray objectAtIndex:i] integerValue]) {
            return false;
        }
    }

    if (![letterA isEqualToString:@""] && ![letterB isEqualToString:@""]) {
        if ([letterA isEqualToString:letterB]) {
            return false;
        } else {
            if ([letterA integerValue] > [letterB integerValue]) {
                return true;
            } else {
                return false;
            }
        }
    } else if (![letterA isEqualToString:@""]) {
        return true;
    } else if (![letterB isEqualToString:@""]) {
        return false;
    } else {
        return false;
    }
}

+ (NSArray *)normaliseValuesFromArray:(NSArray *)array{
    if ([array count] < maxValues){
        NSMutableArray *mutableArray = [array mutableCopy];
        NSInteger difference = maxValues-[mutableArray count];
        for (NSInteger i=0; i<difference; i++) {
            [mutableArray addObject:@"0"];
        }
        return mutableArray;
    } else {
        return array;
    }
}

@end
